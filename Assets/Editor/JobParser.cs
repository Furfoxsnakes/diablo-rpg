﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using System;
using System.IO;

public static class JobParser {
    [MenuItem("PreProduction/Parse Jobs")]
    public static void Parse()
    {
        CreateDirectories();
        ParseStartingStats();
        ParseGrowthStats();
    }

    private static void CreateDirectories()
    {
        if (!AssetDatabase.IsValidFolder("Assets/Resources/Jobs"))
            AssetDatabase.CreateFolder("Assets/Resources", "Jobs");
    }

    private static void ParseStartingStats()
    {
        string readPath = string.Format("{0}/Settings/JobStartingStats.csv", Application.dataPath);
        string[] readText = File.ReadAllLines(readPath);
        for (int i = 1; i < readText.Length; ++i)
            ParseStartingStats(readText[i]);
    }

    private static void ParseStartingStats(string line)
    {
        string[] elements = line.Split(',');
        GameObject obj = GetOrCreate(elements[0]);
        Job job = obj.GetComponent<Job>();
        for (int i = 1; i < Job.StatOrder.Length + 1; ++i)
          job.BaseStats[i-1] = Convert.ToInt32(elements[i]);

        StatModifierFeature move = GetFeature (obj, StatTypes.MOV);
        move.amount = Convert.ToInt32(elements[8]);
    }

    static void ParseGrowthStats ()
	{
		string readPath = string.Format("{0}/Settings/JobGrowthStats.csv", Application.dataPath);
		string[] readText = File.ReadAllLines(readPath);
		for (int i = 1; i < readText.Length; ++i)
			ParseGrowthStats(readText[i]);
	}

	static void ParseGrowthStats (string line)
	{
		string[] elements = line.Split(',');
		GameObject obj = GetOrCreate(elements[0]);
		Job job = obj.GetComponent<Job>();
		for (int i = 1; i < elements.Length; ++i)
        	job.GrowthStats[i-1] = Convert.ToSingle(elements[i]);

	}

    private static StatModifierFeature GetFeature(GameObject obj, StatTypes type)
    {
        StatModifierFeature[] smf = obj.GetComponents<StatModifierFeature>();
        for (int i = 0; i < smf.Length; ++i)
            if (smf[i].type == type)
                return smf[i];

        StatModifierFeature feature = obj.AddComponent<StatModifierFeature>();
        feature.type = type;
        return feature;
    }

    private static GameObject GetOrCreate(string jobName)
    {
        string fullPath = string.Format("Assets/Resources/Jobs/{0}.prefab", jobName);
        GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(fullPath);
        if (obj == null)
            obj = Create(fullPath);

        return obj;
    }

    private static GameObject Create(string fullPath)
    {
        GameObject instance = new GameObject("temp");
        instance.AddComponent<Job>();
        GameObject prefab = PrefabUtility.CreatePrefab(fullPath, instance);
        GameObject.DestroyImmediate(instance);
        return prefab;
    }
}
