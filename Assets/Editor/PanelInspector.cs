﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Panel))]
public class PanelInspector : Editor {

	public Panel current { get { return target as Panel; } }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }
}
