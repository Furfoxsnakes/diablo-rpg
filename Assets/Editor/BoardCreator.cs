﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.IO;

public class BoardCreator : MonoBehaviour {

    [SerializeField] GameObject tileViewPrefab;
    [SerializeField] GameObject tileSelectionIndicatorPrefab;
    [SerializeField] string levelName;

    Transform marker
    {
        get
        {
            if (_marker == null)
            {
                GameObject instance = Instantiate(tileSelectionIndicatorPrefab) as GameObject;
                _marker = instance.transform;
            }
            return _marker;
        }
    }
    Transform _marker;

    Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();

    [SerializeField] int width = 10;
    [SerializeField] int depth = 10;
    [SerializeField] float height = 8;
    [SerializeField] Point pos;
    [SerializeField] LevelData levelData;

    #region Public
    public void GrowArea()
    {
        Rect r = RandomRect();
        GrowRect(r);
    }

    public void ShrinkArea()
    {
        Rect r = RandomRect();
        ShrinkRect(r);
    }

    public void Grow()
    {
        GrowSingle(pos);
    }

    public void Shrink()
    {
        ShrinkSingle(pos);
    }

    public void UpdateMarker()
    {
        Tile t = tiles.ContainsKey(pos) ? tiles[pos] : null;
        marker.localPosition = t != null ? t.Center : new Vector3(pos.x, 0, pos.y);
    }

    /// <summary>
    /// Deletes all the tile game objects and clears the tiles list
    /// </summary>
    public void Clear()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
            DestroyImmediate(transform.GetChild(i).gameObject);

        tiles.Clear();
    }

    /// <summary>
    /// Saves the board creator to a LevelData scriptable object
    /// </summary>
    public void Save()
    {
        string filePath = Application.dataPath + "/Resources/Levels";
        if (!Directory.Exists(filePath))
            CreateSaveDirectory();

        LevelData board = ScriptableObject.CreateInstance<LevelData>();
        board.Tiles = new List<Vector3>(tiles.Count);
        foreach (Tile t in tiles.Values)
            board.Tiles.Add(new Vector3(t.Pos.x, t.Height, t.Pos.y));

        string fileName = string.Format("Assets/Resources/Levels/{1}.asset", filePath, name);
        AssetDatabase.CreateAsset(board, fileName);
    }

    public void Load()
    {
        Clear();

        if (levelData == null)
            return;

        foreach(Vector3 v in levelData.Tiles)
        {
            Tile t = Create();
            t.Load(v);
            tiles.Add(t.Pos, t);
        }
    }
    #endregion

    #region Private
    private Rect RandomRect()
    {
        int x = UnityEngine.Random.Range(0, width);
        int y = UnityEngine.Random.Range(0, width);
        int w = UnityEngine.Random.Range(1, width - x + 1);
        int h = UnityEngine.Random.Range(1, depth - y + 1);
        return new Rect(x, y, w, h);
    }

    private void GrowRect(Rect r)
    {
        for (int y = (int)r.yMin; y < (int)r.yMax; y++)
            for (int x = (int)r.xMin; x < (int)r.xMax; x++)
            {
                Point p = new Point(x, y);
                GrowSingle(p);
            }
    }

    private void ShrinkRect(Rect r)
    {
        for (int y = (int)r.yMin; y < (int)r.yMax; y++)
            for (int x = (int)r.xMin; x < (int)r.xMax; x++)
            {
                Point p = new Point(x, y);
                ShrinkSingle(p);
            }
    }

    private Tile Create()
    {
        GameObject instance = Instantiate(tileViewPrefab) as GameObject;
        instance.transform.SetParent(transform);
        return instance.GetComponent<Tile>();
    }

    private Tile GetOrCreate(Point p)
    {
        if (tiles.ContainsKey(p))
            return tiles[p];

        Tile t = Create();
        t.Load(p, 0);
        tiles.Add(p, t);

        return t;
    }

    private void GrowSingle(Point p)
    {
        Tile t = GetOrCreate(p);

        // grow the tile if it's not at the max height already
        if (t.Height < height)
            t.Grow();
    }

    private void ShrinkSingle(Point p)
    {
        // don't do anything if this position doesn't exist in the list
        if (!tiles.ContainsKey(p))
            return;

        Tile t = tiles[p];
        t.Shrink();

        // remove this tile if the height get reduced to 0 or less
        if (t.Height <= 0)
        {
            tiles.Remove(p);
            DestroyImmediate(t.gameObject);
        }
    }

    private void CreateSaveDirectory()
    {
        string filePath = Application.dataPath + "/Resources";
        // create a "Resources" directory if it doesn't exist
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets", "Resources");

        filePath += "/Levels";

        // create a "Levels directory if it doesn't exist
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets/Resources", "Levels");

        AssetDatabase.Refresh();
    }
    #endregion
}
