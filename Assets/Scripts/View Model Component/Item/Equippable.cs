﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equippable : MonoBehaviour {

    #region Fields
    public EquipSlots DefaultSlots;
    public EquipSlots SecondarySlots;
    public EquipSlots Slots;
    bool isEquipped;
    #endregion

    #region Public
    public void OnEquip()
    {
        if (isEquipped)
            return;

        isEquipped = true;

        Feature[] features = GetComponentsInChildren<Feature>();
        for (int i = 0; i < features.Length; ++i)
            features[i].Activate(gameObject);
    }

    public void OnUnEquip()
    {
        if (!isEquipped)
            return;

        isEquipped = false;

        Feature[] features = GetComponentsInChildren<Feature>();
        for (int i = 0; i < features.Length; ++i)
            features[i].Deactivate();
    }
    #endregion
}
