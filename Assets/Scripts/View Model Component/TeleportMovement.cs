﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TeleportMovement : Movement
{
    public override IEnumerator Traverse(Tile tile)
    {
        unit.Place(tile);

        jumper.DOLocalRotate(new Vector3(0, 360, 0), 0.5f);
        var shrinkTween = transform.DOScale(Vector3.zero, 0.5f);
        yield return shrinkTween.WaitForKill();

        transform.position = tile.Center;

        var growTween = transform.DOScale(Vector3.one, 0.5f);
        yield return growTween.WaitForKill();
    }
}
