﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Feature : MonoBehaviour {

    #region Fields
    protected GameObject Target { get; private set; }
    #endregion

    #region Public
    public void Activate (GameObject target)
	{
		if (Target == null)
		{
			Target = target;
			OnApply();
		}
	}

    public void Deactivate()
    {
        if (Target != null)
        {
            OnRemove();
            Target = null;
        }
    }

    public void Apply(GameObject target)
    {
        Target = target;
        OnApply();
        Target = null;
    }
    #endregion

    #region Private
    protected abstract void OnApply();
    protected virtual void OnRemove(){ }
    #endregion
}
