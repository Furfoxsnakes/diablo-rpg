﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatModifierFeature : Feature
{
    #region Fields
    public StatTypes type;
    public int amount;

    Stats Stats { get { return Target.GetComponent<Stats>(); }}
    #endregion

    #region Protected
    protected override void OnApply()
    {
        Stats[type] += amount;
    }

    protected override void OnRemove()
    {
        Stats[type] -= amount;
    }
    #endregion

}
