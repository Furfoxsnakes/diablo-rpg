﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

	public int this[StatTypes s]
    {
        get { return data[(int)s]; }
        set { SetValue(s, value, true);}
    }
    int[] data = new int[(int)StatTypes.Count];

    static Dictionary<StatTypes, string> willChangeNotifications = new Dictionary<StatTypes, string>();
    static Dictionary<StatTypes, string> didChangeNotifications = new Dictionary<StatTypes, string>();

    public static string WillChangeNotification(StatTypes type)
    {
        if (!willChangeNotifications.ContainsKey(type))
            willChangeNotifications.Add(type, string.Format("Stats.{0}WillChange", type.ToString()));

        return willChangeNotifications[type];
    }

    public static string DidChangeNotification(StatTypes type)
    {
        if (!didChangeNotifications.ContainsKey(type))
            didChangeNotifications.Add(type, string.Format("Stats.{0}DidChange", type.ToString()));

        return didChangeNotifications[type];
    }

    public void SetValue (StatTypes type, int value, bool allowExceptions)
    {
        int oldValue = this[type];

        if (oldValue == value)
            return;

        if (allowExceptions)
        {
            ValueChangeException exc = new ValueChangeException(oldValue, value);

            this.PostNotification(WillChangeNotification(type), exc);

            value = Mathf.FloorToInt(exc.GetModifiedValue());

            // check if something nullified the change or the value is the same as the initial value
            if (exc.Toggle == false || value == oldValue)
                return;
        }

        data[(int)type] = value;

        this.PostNotification(DidChangeNotification(type), oldValue);
    }
}
