﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour {

    public float Speed = 3f;
    public Transform follow;
    Transform _transform;

    private void Start()
    {
        _transform = transform;
    }

    // Update is called once per frame
    void Update () {
        if (follow)
            _transform.position = Vector3.Lerp(_transform.position, follow.position, Speed * Time.deltaTime);
	}
}
