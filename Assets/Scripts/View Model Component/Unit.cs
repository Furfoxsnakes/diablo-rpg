﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    #region Fields/Properties
    public Tile Tile { get; protected set; }
    public Directions Dir;
    #endregion

    #region Funcs

    #region Public
    /// <summary>
    /// Place unit at a target tile
    /// </summary>
    /// <param name="target">Tile to place unit at</param>
    public void Place(Tile target)
    {
        // clear the previous tile this unit was pointing to if available
        if (Tile != null && Tile.Content == gameObject)
            Tile.Content = null;

        Tile = target;

        // set a reference on the tile target to this game object
        if (target != null)
            target.Content = gameObject;
    }

    /// <summary>
    /// Match the game object to the center and proper rotation
    /// </summary>
    public void Match()
    {
        transform.localPosition = Tile.Center;
        transform.localEulerAngles = Dir.ToEuler();
    }
    #endregion

    #endregion
}
