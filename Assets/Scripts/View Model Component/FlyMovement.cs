﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FlyMovement : Movement
{
    public override IEnumerator Traverse(Tile tile)
    {
        // get the distance between this unit and the target tile
        // http://theliquidfire.com/2015/06/08/tactics-rpg-path-finding/
        float dist = Mathf.Sqrt(Mathf.Pow(tile.Pos.x - unit.Tile.Pos.x, 2) + Mathf.Pow(tile.Pos.y - unit.Tile.Pos.y, 2));

        unit.Place(tile);

        // fly high enough not to clip through tiles
        float y = Tile.StepHeight * 10;
        float duration = (y - jumper.position.y) * 0.5f;

        var tweener = jumper.transform.DOLocalMove(new Vector3(0, y, 0), duration);
        yield return tweener.WaitForKill();

        // turn to target direction
        Directions dir;
        Vector3 toTile = (tile.Center - transform.position);
        if (Mathf.Abs(toTile.x) > Mathf.Abs(toTile.z))
            dir = toTile.x > 0 ? Directions.East : Directions.West;
        else
            dir = toTile.z > 0 ? Directions.North : Directions.South;
        yield return StartCoroutine(Turn(dir));

        // move to target position
        tweener = transform.DOMove(tile.Center, dist * 0.5f);
        yield return tweener.WaitForKill();

        // Land on tile
        duration = (y - tile.Center.y) * 0.5f;
        tweener = jumper.DOLocalMove(Vector3.zero, 0.5f);
        yield return tweener.WaitForKill();
    }
}
