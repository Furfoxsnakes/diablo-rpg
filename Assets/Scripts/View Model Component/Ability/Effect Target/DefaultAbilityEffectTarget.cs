﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultAbilityEffectTarget : AbilityEffectTarget
{
    public override bool IsTarget(Tile tile)
    {
        if (tile == null || tile.Content == null)
            return false;

        Stats s = tile.Content.GetComponent<Stats>();
        return s != null && s[StatTypes.HP] > 0;
    }
}
