﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecifyAbilityArea : AbilityArea
{
    public int Horizontal;
    private Tile tile;

    public override List<Tile> GetTilesInArea(Board board, Point pos)
    {
        tile = board.GetTile(pos);
        return board.Search(tile, ExpandSearch);
    }

    private bool ExpandSearch(Tile to, Tile from)
    {
        return (to.Distance + 1) <= Horizontal;
    }
}
