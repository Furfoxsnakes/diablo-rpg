﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityArea : MonoBehaviour {
    protected Unit Unit { get { return GetComponentInParent<Unit>();}}
	public abstract List<Tile> GetTilesInArea(Board board, Point pos);
}
