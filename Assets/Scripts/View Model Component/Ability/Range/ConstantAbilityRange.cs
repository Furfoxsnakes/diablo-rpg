﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantAbilityRange : AbilityRange
{
    public override List<Tile> GetTilesInRange(Board board)
    {
        return board.Search(Unit.Tile, ExpandSearch);
    }

    private bool ExpandSearch(Tile from, Tile to)
    {
        return (from.Distance + 1) <= Horizontal;
    }
}
