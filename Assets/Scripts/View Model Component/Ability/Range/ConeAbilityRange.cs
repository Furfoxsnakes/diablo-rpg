﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeAbilityRange : AbilityRange
{
    public override bool DirectionalOriented { get { return true;}}

    public override List<Tile> GetTilesInRange(Board board)
    {
        Point pos = Unit.Tile.Pos;
        List<Tile> retValue = new List<Tile>();
        int dir = (Unit.Dir == Directions.North || Unit.Dir == Directions.East) ? 1 : -1;
        int lateral = 1;

        if (Unit.Dir == Directions.North || Unit.Dir == Directions.South)
        {
            for (int y = 1; y <= Horizontal; ++y)
            {
                int min = -(lateral / 2);
                int max = lateral / 2;

                for (int x = min; x <= max; ++x)
                {
                    Point next = new Point(pos.x + x, pos.y + (y * dir));
                    Tile tile = board.GetTile(next);
                    if (tile != null)
                        retValue.Add(tile);
                }

                lateral += 2;
            }
        }
        else
        {
            for (int x = 1; x <= Horizontal; ++x)
            {
                int min = -(lateral / 2);
                int max = lateral / 2;

                for (int y = min; y <= max; ++y)
                {
                    Point next = new Point(pos.x + (x * dir), pos.y + y);
                    Tile tile = board.GetTile(next);
                    if (tile != null)
                        retValue.Add(tile);
                }

                lateral += 2;
            }
        }

        return retValue;
    }
}
