﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineAbilityRange : AbilityRange
{
    public override bool DirectionalOriented { get { return true; }}

    public override List<Tile> GetTilesInRange(Board board)
    {
        Point startPos = Unit.Tile.Pos;
        Point endPos;
        List<Tile> retValue = new List<Tile>();

        switch (Unit.Dir)
        {
            case Directions.North:
                endPos = new Point(startPos.x, board.Max.y);
                break;
            case Directions.East:
                endPos = new Point(board.Max.x, startPos.y);
                break;
            case Directions.South:
                endPos = new Point(startPos.x, board.Min.y);
                break;
            default:    // West
                endPos = new Point(board.Min.x, startPos.y);
                break;
        }

        while (startPos != endPos)
        {
            if (startPos.x < endPos.x)
                startPos.x++;
            else if (startPos.x > endPos.x)
                startPos.x--;

            if (startPos.y < endPos.y)
                startPos.y++;
            else if (startPos.y > endPos.y)
                startPos.y--;

            Tile t = board.GetTile(startPos);
            if (t != null)
                retValue.Add(t);
        }

        return retValue;
    }
}
