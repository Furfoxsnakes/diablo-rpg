﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityRange : MonoBehaviour {

	public int Horizontal = 1;
    public virtual bool DirectionalOriented { get { return false;}}
    protected Unit Unit { get { return GetComponentInParent<Unit>();}}

    public abstract List<Tile> GetTilesInRange (Board board);
}
