﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {

    #region Fields/Properties
    [SerializeField] GameObject tilePrefab;
    public Dictionary<Point, Tile> Tiles = new Dictionary<Point, Tile>();
    public Point Min { get { return min;}}
    public Point Max { get { return max;}}
    private Point min, max;

    [SerializeField] Color selectedTileColour = new Color(0, 1, 1, 1);
    [SerializeField] Color defaultTileColour = new Color(1, 1, 1, 1);

    private readonly Point[] dirs = new Point[4]
    {
        new Point(0, 1),
        new Point(0, -1),
        new Point(1, 0),
        new Point(-1, 0)
    };
    #endregion

    #region Funcs

    #region Public
    public void Load(LevelData data)
    {
        min = new Point(int.MaxValue, int.MaxValue);
        max = new Point(int.MinValue, int.MinValue);

        for (int i = 0; i < data.Tiles.Count; i++)
        {
            GameObject instance = Instantiate(tilePrefab) as GameObject;
            Tile t = instance.GetComponent<Tile>();
            t.Load(data.Tiles[i]);
            Tiles.Add(t.Pos, t);

            min.x = Mathf.Min(min.x, t.Pos.x);
            min.y = Mathf.Min(min.y, t.Pos.y);
            max.x = Mathf.Max(max.x, t.Pos.x);
            max.y = Mathf.Max(max.y, t.Pos.y);
        }
    }

    public List<Tile> Search (Tile start, Func<Tile, Tile, bool> addTile)
    {
        List<Tile> retValue = new List<Tile>
        {
            start
        };

        ClearSearch();
        Queue<Tile> checkNext = new Queue<Tile>();
        Queue<Tile> checkNow = new Queue<Tile>();

        start.Distance = 0;
        checkNow.Enqueue(start);

        while (checkNow.Count > 0)
        {
            Tile t = checkNow.Dequeue();
            
            for (int i = 0; i < 4; i++)
            {
                Tile next = GetTile(t.Pos + dirs[i]);

                // dont' add the tile if it's not avaiable or it's distance is already lower
                if (next == null || next.Distance <= t.Distance + 1)
                    continue;

                if (addTile(t, next))
                {
                    next.Distance = t.Distance + 1;
                    next.Prev = t;
                    checkNext.Enqueue(next);
                    retValue.Add(next);
                }
            }

            // if the checkNow queue has been emptied
            // swap the checkNext queue into the checkNow queue
            if (checkNow.Count == 0)
                SwapReference(ref checkNow, ref checkNext);
        }

        return retValue;
    }

    public Tile GetTile(Point p)
    {
        return Tiles.ContainsKey(p) ? Tiles[p] : null;
    }

    public void SelectTiles(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
            tiles[i].GetComponent<Renderer>().material.SetColor("_Color", selectedTileColour);
    }
    public void DeselectTiles(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
            tiles[i].GetComponent<Renderer>().material.SetColor("_Color", defaultTileColour);
    }
    #endregion

    #region Private
    private void ClearSearch()
    {
        foreach (Tile t in Tiles.Values)
        {
            t.Prev = null;
            t.Distance = int.MaxValue;
        }
    }

    private void SwapReference(ref Queue<Tile> a, ref Queue<Tile> b)
    {
        Queue<Tile> temp = a;
        a = b;
        b = temp;
    }
    #endregion

    #endregion

}
