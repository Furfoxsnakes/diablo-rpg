﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WalkMovement : Movement
{
    #region Funcs

    #region Public
    public override IEnumerator Traverse(Tile tile)
    {
        unit.Place(tile);

        // build a list of waypoints for the unit to travel through
        List<Tile> targets = new List<Tile>();
        while (tile != null)
        {
            targets.Insert(0, tile);
            tile = tile.Prev;
        }

        // move through each tile/waypoint in succession
        for(int i = 1; i < targets.Count; i++)
        {
            Tile from = targets[i - 1];
            Tile to = targets[i];

            Directions dir = from.GetDirection(to);
            if (unit.Dir != dir)
                yield return StartCoroutine(Turn(dir));

            if (from.Height == to.Height)
                yield return StartCoroutine(Walk(to));
            else
                yield return StartCoroutine(Jump(to));
        }

        yield return null;
    }
    #endregion

    #region Private
    protected override bool ExpandSearch(Tile from, Tile to)
    {
        // tile isn't valid if the height is more than the jumper's allowed height 
        //if (Mathf.Abs(from.Height - to.Height) > JumpHeight) 
        //    return false; 

        // tile isn't valid if the tile is being blocked by content
        if (to.Content != null)
            return false;

        // otherwise run a default range check
        return base.ExpandSearch(from, to);
    }

    private IEnumerator Walk (Tile target)
    {
        var tweener = transform.DOMove(target.Center, 0.5f);
        yield return tweener.WaitForKill();
    }

    private IEnumerator Jump (Tile target)
    {
        var tweener = transform.DOJump(target.Center, 1f, 1, 0.5f);
        yield return tweener.WaitForKill();
    }
    #endregion

    #endregion
}
