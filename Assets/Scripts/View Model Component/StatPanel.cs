﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatPanel : MonoBehaviour {

    #region Fields
    public Panel Panel;
    public Sprite AllyBackground;
    public Sprite EnemyBackground;
    public Image Background;
    public Image Avatar;
    public TextMeshProUGUI NameLabel;
    public TextMeshProUGUI HpLabel;
    public TextMeshProUGUI MpLabel;
    public TextMeshProUGUI LvlLabel;
    #endregion

    #region Public
    public void Display(GameObject obj)
    {
        Background.sprite = Random.value > 0.5f ? EnemyBackground : AllyBackground;
        // Avatar.sprite = null;
        NameLabel.text = obj.name;

        Stats stats = obj.GetComponent<Stats>();
        if (stats)
        {
            HpLabel.text = string.Format("HP {0} / {1}", stats[StatTypes.HP], stats[StatTypes.MHP]);
            MpLabel.text = string.Format("MP {0} / {1}", stats[StatTypes.MP], stats[StatTypes.MMP]);
            LvlLabel.text = string.Format("LV. {0}", stats[StatTypes.LVL]);
        }
    }
    #endregion
}
