﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rank : MonoBehaviour {

	public const int MIN_LEVEL = 1;
    public const int MAX_LEVEL = 99;
    public const int MAX_EXPERIENCE = 999999;

    public int LVL
    {
        get { return stats[StatTypes.LVL]; }
    }

    public int EXP
    {
        get { return stats[StatTypes.EXP]; }
        set { stats[StatTypes.EXP] = value; }
    }

    public float LevelPercent
    {
        get { return (float)(LVL - MIN_LEVEL) / (float)(MAX_LEVEL - MIN_LEVEL); }
    }

    Stats stats;

    private void Awake()
    {
        stats = GetComponent<Stats>();
    }

    private void OnEnable()
    {
        this.AddObserver(OnExpWillChange, Stats.WillChangeNotification(StatTypes.EXP), stats);
        this.AddObserver(OnExpDidChange, Stats.DidChangeNotification(StatTypes.EXP), stats);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnExpWillChange, Stats.WillChangeNotification(StatTypes.EXP), stats);
        this.RemoveObserver(OnExpDidChange, Stats.DidChangeNotification(StatTypes.EXP), stats);
    }

    public static int ExperienceForLevel(int level)
    {
        float levelPercent = Mathf.Clamp01((float)(level - MIN_LEVEL) / (float)(MAX_LEVEL - MIN_LEVEL));
        return (int)EasingEquations.EaseInQuad(0, MAX_EXPERIENCE, levelPercent);
    }

    public static int LevelForExperience (int xp)
    {
        int lvl = MAX_LEVEL;

        for (; lvl >= MIN_LEVEL; --lvl)
            if (xp >= ExperienceForLevel(lvl))
                break;

        return lvl;
    }

    private void OnExpWillChange(object sender, object args)
    {
        ValueChangeException vce = args as ValueChangeException;
        vce.AddModifier(new ClampValueModifier(int.MaxValue, EXP, MAX_EXPERIENCE));
    }

    private void OnExpDidChange(object sender, object args)
    {
        stats.SetValue(StatTypes.LVL, LevelForExperience(EXP), false);
    }

    public void Init (int level)
    {
        stats.SetValue(StatTypes.LVL, level, false);
        stats.SetValue(StatTypes.EXP, ExperienceForLevel(level), false);
    }
}
