﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Job : MonoBehaviour {

    #region Fields
    public static readonly StatTypes[] StatOrder = new StatTypes[]
    {
        StatTypes.MHP,
        StatTypes.MMP,
        StatTypes.ATK,
        StatTypes.DEF,
        StatTypes.MAT,
        StatTypes.MDF,
        StatTypes.SPD
    };

    public int[] BaseStats = new int[StatOrder.Length];
    public float[] GrowthStats = new float[StatOrder.Length];
    private Stats stats;
    #endregion

    #region Monobehaviour
    private void OnDestroy()
    {
        this.RemoveObserver(OnLvlChangeNotification, Stats.DidChangeNotification(StatTypes.LVL));
    }
    #endregion

    #region Public
    public void Employ()
    {
        stats = gameObject.GetComponentInParent<Stats>();
        this.AddObserver(OnLvlChangeNotification, Stats.DidChangeNotification(StatTypes.LVL));

        Feature[] features = GetComponentsInChildren<Feature>();
        for (int i = 0; i < features.Length; ++i)
        {
            features[i].Activate(transform.parent.gameObject);
        }
    }

    public void UnEmploy()
    {
        Feature[] features = GetComponentsInChildren<Feature>();
        for (int i = 0; i < features.Length; ++i)
            features[i].Deactivate();

        this.RemoveObserver(OnLvlChangeNotification, Stats.DidChangeNotification(StatTypes.LVL));
        stats = null;
    }

    public void LoadDefaultStats()
    {
        for (int i = 0; i < StatOrder.Length; ++i)
        {
            StatTypes type = StatOrder[i];
            stats.SetValue(type, BaseStats[i], false);
        }

        stats.SetValue(StatTypes.HP, stats[StatTypes.MHP], false);
        stats.SetValue(StatTypes.MP, stats[StatTypes.MMP], false);
    }
    #endregion

    #region Event Handlers
    protected virtual void OnLvlChangeNotification(object sender, object args)
    {
        int oldValue = (int)args;
        int newValue = stats[StatTypes.LVL];

        for (int i = oldValue; i < newValue; ++i)
            LevelUp();

    }
    #endregion

    #region Private
    private void LevelUp()
    {
        for (int i = 0; i < StatOrder.Length; ++i)
        {
            StatTypes type = StatOrder[i];
            int whole = Mathf.FloorToInt(GrowthStats[i]);
            float fraction = GrowthStats[i] - whole;

            int value = stats[type];
            value += whole;
            if (Random.value <= fraction)
                value++;

            stats.SetValue(type, value, false);
        }

        // restore all HP & MP on level up
        stats.SetValue(StatTypes.HP, stats[StatTypes.MHP], false);
        stats.SetValue(StatTypes.MP, stats[StatTypes.MMP], false);
    }
    #endregion
}
