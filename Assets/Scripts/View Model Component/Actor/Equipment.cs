﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour {

    #region Notifications
    public const string EQUIPPED_NOTIFICATION = "Equipment.EquippedNotification";
    public const string UNEQUIPPED_NOTIFICATION = "Equipment.UnEquippedNotification";
    #endregion

    #region Fields
    public IList<Equippable> Items { get { return items.AsReadOnly();}}
    List<Equippable> items = new List<Equippable>();
    #endregion

    #region Public
    public void Equip (Equippable item, EquipSlots slots)
    {
        UnEquip(slots);

        items.Add(item);
        item.transform.SetParent(transform);
        item.Slots = slots;
        item.OnEquip();

        this.PostNotification(EQUIPPED_NOTIFICATION, item);
    }

    public void UnEquip(Equippable item)
    {
        item.OnUnEquip();
        item.Slots = EquipSlots.None;
        item.transform.SetParent(transform);
        items.Remove(item);

        this.PostNotification(UNEQUIPPED_NOTIFICATION, item);
    }

    public void UnEquip(EquipSlots slots)
    {
        for (int i = items.Count - 1; i >= 0; --i)
        {
            Equippable item = items[i];
            if ((item.Slots & slots) != EquipSlots.None)
                UnEquip(item);
        }
    }
    #endregion
}
