﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    #region Fields/Properties
    public const float StepHeight = 0.25f;

    public Point Pos;
    public int Height;
    public GameObject Content;

    public Tile Prev;
    public int Distance;

    public Vector3 Center { get { return new Vector3(Pos.x, Height * StepHeight, Pos.y); } }
    #endregion

    #region Funcs

    #region Public
    public void Grow()
    {
        Height++;
        Match();
    }

    public void Shrink()
    {
        Height--;
        Match();
    }

    public void Load(Point p, int h)
    {
        Pos = p;
        Height = h;
        Match();
    }

    public void Load(Vector3 v)
    {
        Load(new Point((int)v.x, (int)v.z), (int)v.y);
    }
    #endregion

    #region Private
    private void Match()
    {
        transform.localPosition = new Vector3(Pos.x, Height * StepHeight / 2f, Pos.y);
        transform.localScale = new Vector3(1f, Height * StepHeight, 1f);
    }
    #endregion

    #endregion

}
