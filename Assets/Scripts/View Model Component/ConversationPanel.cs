﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class ConversationPanel : MonoBehaviour {

    public TMP_Text Message;
    public Image Speaker;
    public GameObject Arrow;
    public Panel Panel;

	// Use this for initialization
	void Start () {
        Vector3 arrowPos = Arrow.transform.localPosition;
        Arrow.transform.localPosition = new Vector3(arrowPos.x, arrowPos.y + 5, arrowPos.z);
        Arrow.transform.DOLocalMove(new Vector3(arrowPos.x, arrowPos.y - 5, arrowPos.z), 0.5f).SetLoops(-1, LoopType.Yoyo);
	}
	
	public IEnumerator Display (SpeakerData data)
    {
        Speaker.sprite = data.speaker;
        //Speaker.SetNativeSize();

        for (int i = 0; i < data.messages.Count; ++i)
        {
            Message.text = data.messages[i];
            // set the arrow active if there's another message in the list
            Arrow.SetActive(i + 1 < data.messages.Count);
            yield return null;
        }
    }
}
