﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Unit))]
public abstract class Movement : MonoBehaviour {

    #region Fields/Properties
    public int Range { get { return stats[StatTypes.MOV];}}
    public int JumpHeight;
    protected Unit unit;
    protected Transform jumper;
    protected Stats stats;
    #endregion

    #region Funcs

    #region Monobehaviour
    protected virtual void Awake()
    {
        unit = GetComponent<Unit>();
        jumper = transform.Find("Jumper");
        stats = GetComponent<Stats>();
    }
    #endregion

    #region Public
    /// <summary>
    /// Get a list of tiles in range of this unit
    /// </summary>
    /// <param name="board">Board containing tiles</param>
    /// <returns></returns>
    public virtual List<Tile> GetTilesInRange(Board board)
    {
        // get a list of tiles in range
        List<Tile> retValue = board.Search(unit.Tile, ExpandSearch);
        // filter out invalid tiles
        Filter(retValue);
        return retValue;
    }

    /// <summary>
    /// Handle the animation of traversing the unit to the specified tile
    /// </summary>
    /// <param name="tile">Tile to traverse to</param>
    /// <returns></returns>
    public abstract IEnumerator Traverse(Tile tile);
    #endregion

    #region Private

    /// <summary>
    /// Returns wether a tile is in range
    /// </summary>
    /// <param name="from">Starting tile</param>
    /// <param name="to">Target tile</param>
    /// <returns></returns>
    protected virtual bool ExpandSearch(Tile from, Tile to)
    {
        return (from.Distance + 1) <= Range;
    }

    /// <summary>
    /// Removes tiles that cannot be moved to
    /// eg. tiles with content
    /// </summary>
    /// <param name="tiles">List of tiles to filter through</param>
    protected virtual void Filter (List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
            if (tiles[i].Content != null)
                tiles.RemoveAt(i);
    }

    protected virtual IEnumerator Turn (Directions dir)
    {
        var tween = transform.DORotate(dir.ToEuler(), 0.25f);
        yield return tween.WaitForKill();
    }
    #endregion

    #endregion

}
