﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatPanelController : MonoBehaviour {

    #region Consts
    const string ShowKey = "Show";
    const string HideKey = "Hide";
    #endregion

    #region Fields
    [SerializeField] private StatPanel primaryPanel;
    [SerializeField] private StatPanel secondaryPanel;

    private Tweener primaryTransition;
    private Tweener secondaryTransition;
    #endregion

    #region Mono
    private void Start()
    {
        if (primaryPanel.Panel.CurrentPos == null)
            primaryPanel.Panel.SetPosition(HideKey, false);
        if (secondaryPanel.Panel.CurrentPos == null)
            primaryPanel.Panel.SetPosition(HideKey, false);
    }
    #endregion

    #region Public
    public void ShowPrimary(GameObject obj)
    {
        primaryPanel.Display(obj);
        MovePanel(primaryPanel, ShowKey, ref primaryTransition);
    }

    public void HidePrimary()
    {
        MovePanel(primaryPanel, HideKey, ref primaryTransition);
    }

    public void ShowSecondary(GameObject obj)
    {
        secondaryPanel.Display(obj);
        MovePanel(secondaryPanel, ShowKey, ref secondaryTransition);
    }

    public void HideSecondary()
    {
        MovePanel(secondaryPanel, HideKey, ref secondaryTransition);
    }
    #endregion

    #region Private
    private void MovePanel (StatPanel obj, string pos, ref Tweener t)
    {
        Panel.Position target = obj.Panel[pos];

        if (obj.Panel.CurrentPos != null)
        {
            if (t != null && t.easingControl != null)
                t.easingControl.Stop();

            t = obj.Panel.SetPosition(pos, true);
            t.easingControl.duration = 0.5f;
            t.easingControl.equation = EasingEquations.EaseOutQuad;
        }
    }
    #endregion
}
