﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityMenuPanelController : MonoBehaviour {

    const string ShowKey = "Show";
    const string HideKey = "Hide";
    const string EntryPoolKey = "AbilityMenuPanel.Entry";
    const int MenuCount = 4;

    [SerializeField] GameObject entryPrefab;
    [SerializeField] TMPro.TextMeshProUGUI titleLabel;
    [SerializeField] Panel panel;
    [SerializeField] GameObject canvas;

    List<AbilityMenuEntry> menuEntries = new List<AbilityMenuEntry>(MenuCount);
    public int Selection { get; private set; }

    private void Awake()
    {
        GameObjectPoolController.AddEntry(EntryPoolKey, entryPrefab, MenuCount, int.MaxValue);
    }

    private AbilityMenuEntry Dequeue()
    {
        Poolable p = GameObjectPoolController.Dequeue(EntryPoolKey);
        AbilityMenuEntry entry = p.GetComponent<AbilityMenuEntry>();
        entry.transform.SetParent(panel.transform, false);
        entry.transform.localScale = Vector3.one;
        entry.gameObject.SetActive(true);
        entry.Reset();
        return entry;
    }

    private void Enqueue(AbilityMenuEntry entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        GameObjectPoolController.Enqueue(p);
    }

    private void Clear()
    {
        for (int i = menuEntries.Count - 1; i >= 0; --i)
            Enqueue(menuEntries[i]);

        menuEntries.Clear();
    }

    private void Start()
    {
        panel.SetPosition(HideKey, false);
        canvas.SetActive(false);
    }

    private Tweener TogglePos (string pos)
    {
        Tweener t = panel.SetPosition(pos, true);
        t.easingControl.duration = 0.5f;
        t.easingControl.equation = EasingEquations.EaseOutQuad;
        return t;
    }

    private bool SetSelection (int value)
    {
        if (menuEntries[value].IsLocked)
            return false;

        // Deselet the previously selected entry
        if (Selection >= 0 && Selection < menuEntries.Count)
            menuEntries[Selection].IsSelected = false;

        Selection = value;

        // Select new entry
        if (Selection >= 0 && Selection < menuEntries.Count)
            menuEntries[Selection].IsSelected = true;

        return true;
    }

    public void Next()
    {
        for (int i = Selection + 1; i < Selection + menuEntries.Count; ++i)
        {
            // handles wrapping around to first entry
            int index = i % menuEntries.Count;
            // stop the loop once we're able to select and entry
            if (SetSelection(index))
                break;
        }
    }

    public void Previous()
    {
        for (int i = Selection - 1 + menuEntries.Count; i > Selection; --i)
        {
            int index = i % menuEntries.Count;
            if (SetSelection(index))
                break;
        }
    }

    public void SetLocked(int index, bool isLocked)
    {
        // out of bounds
        if (index < 0 || index >= menuEntries.Count)
            return;

        menuEntries[index].IsLocked = isLocked;

        if (isLocked && Selection == index)
            Next();
    }

    public void Show (string title, List<string> options)
    {
      canvas.SetActive(true);
      Clear ();
      titleLabel.text = title;
      for (int i = 0; i < options.Count; ++i)
      {
        AbilityMenuEntry entry = Dequeue();
        entry.Title = options[i];
        menuEntries.Add(entry);
      }
      SetSelection(0);
      TogglePos(ShowKey);
    }

    public void Hide()
    {
        Tweener t = TogglePos(HideKey);
        t.easingControl.completedEvent += delegate (object sender, System.EventArgs e)
        {
            if (panel.CurrentPos == panel[HideKey])
            {
                Clear();
                canvas.SetActive(false);
            }
        };
    }
}
