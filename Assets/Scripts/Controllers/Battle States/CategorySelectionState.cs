﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategorySelectionState : BaseAbilityMenuState
{
    public override void Enter()
    {
        base.Enter();
        StatPanelController.ShowPrimary(Turn.Actor.gameObject);
    }

    public override void Exit()
    {
        base.Exit();
        StatPanelController.HidePrimary();
    }

    protected override void Cancel()
    {
        owner.ChangeState<CommandSelectionState>();
    }

    protected override void Confirm()
    {
        switch (AbilityMenuPanelController.Selection)
        {
            case 0:
                Attack();
                break;
            case 1:
                SetCategory(0);
                break;
            case 2:
                SetCategory(1);
                break;
        }
    }

    protected override void LoadMenu()
    {
        if (menuOptions == null)
        {
            menuTitle = "Action";
            menuOptions = new List<string>(3)
            {
                "Attack",
                "White Magic",
                "Black Magic"
            };
        }

        AbilityMenuPanelController.Show(menuTitle, menuOptions);
    }

    private void Attack()
    {
        // TODO: add standard attack
        Debug.Log("Standard attack not yet implemented");
        owner.ChangeState<CommandSelectionState>();
    }

    private void SetCategory(int index)
    {
        ActionSelectionState.Category = index;
        owner.ChangeState<ActionSelectionState>();
    }
}
