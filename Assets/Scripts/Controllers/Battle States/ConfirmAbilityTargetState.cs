﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmAbilityTargetState : BattleState {

	private List<Tile> tiles;
    private AbilityArea abilityArea;
    private int index = 0;

    #region Overrides
    public override void Enter()
    {
        base.Enter();
        abilityArea = Turn.Ability.GetComponent<AbilityArea>();
        tiles = abilityArea.GetTilesInArea(Board, Pos);
        Board.SelectTiles(tiles);
        FindTargets();
        RefreshPrimaryStatPanel(Turn.Actor.Tile.Pos);
        SetTarget(0);
    }

    public override void Exit()
    {
        base.Exit();
        Board.DeselectTiles(tiles);
        StatPanelController.HidePrimary();
        StatPanelController.HideSecondary();
    }

    protected override void OnMove(object sender, object args)
    {
        Point p = (Point)args;

        if (p.y > 0 || p.x > 0)
            SetTarget(index + 1);
        else
            SetTarget(index - 1);
    }

    protected override void OnFire(object sender, object args)
    {
        int i = (int)args;

        if (i == 0)
            if (Turn.targets.Count > 0)
                owner.ChangeState<PerformAbilityState>();
        else
            owner.ChangeState<AbilityTargetState>();
    }
    #endregion

    #region Private
    private void FindTargets()
    {
        Turn.targets = new List<Tile>();
        AbilityEffectTarget[] abilityTargets = Turn.Ability.GetComponentsInChildren<AbilityEffectTarget>();

        for (int i = 0; i < tiles.Count; ++i)
            if (IsTarget(tiles[i], abilityTargets))
                Turn.targets.Add(tiles[i]);
    }

    private bool IsTarget(Tile tile, AbilityEffectTarget[] abilityTargets)
    {
        for (int i = 0; i < abilityTargets.Length; ++i)
            if (abilityTargets[i].IsTarget(tile))
                return true;

        return false;
    }

    private void SetTarget(int target)
    {
        index = target;
        if (index < 0) index = Turn.targets.Count - 1;
        if (index >= Turn.targets.Count) index = 0;
        if (Turn.targets.Count > 0) RefreshSecondaryStatPanel(Turn.targets[index].Pos);
    }
    #endregion
}
