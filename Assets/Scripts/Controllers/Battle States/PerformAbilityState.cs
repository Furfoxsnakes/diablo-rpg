﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerformAbilityState : BattleState {

    #region Overrides
    public override void Enter()
    {
        base.Enter();
        Turn.HasUnitActed = true;
        if (Turn.HasUnitMoved)
            Turn.LockMove = true;
        StartCoroutine(Animate());
    }
    #endregion

    #region Private
    IEnumerator Animate()
    {
        // TODO: Play animations
        yield return null;
        // TODO: apply ability effects
        Attack();

        if (Turn.HasUnitMoved)
            owner.ChangeState<EndFacingState>();
        else
            owner.ChangeState<CommandSelectionState>();
    }

    private void Attack()
    {
        for (int i = 0; i < Turn.targets.Count; ++i)
        {
            GameObject obj = Turn.targets[i].Content;
            Stats s = obj != null ? obj.GetComponent<Stats>() : null;

            if (s != null)
            {
                s[StatTypes.HP] -= 50;
                Debug.Log(string.Format("{0} attacked {1} for 50 damage.", Turn.Actor.name, obj));
                if (s[StatTypes.HP] <= 0)
                {
                    Debug.Log("Killed unit!", obj);
                    DestroyImmediate(obj);
                }
            }
        }
    }
    #endregion
}
