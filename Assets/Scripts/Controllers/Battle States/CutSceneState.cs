﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneState : BattleState {

    ConversationController conversationController;
    ConversationData data;

    protected override void Awake()
    {
        base.Awake();
        conversationController = owner.GetComponentInChildren<ConversationController>();
        data = Resources.Load<ConversationData>("Conversations/Intro Scene");
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        // unload the data asset to free up resources
        if (data)
            Resources.UnloadAsset(data);
    }

    public override void Enter()
    {
        base.Enter();
        conversationController.Show(data);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        ConversationController.CompleteEvent += OnCompleteConversation;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        ConversationController.CompleteEvent -= OnCompleteConversation;
    }

    protected override void OnFire(object sender, object args)
    {
        base.OnFire(sender, args);
        conversationController.Next();
    }

    void OnCompleteConversation(object sender, System.EventArgs e)
    {
        owner.ChangeState<SelectUnitState>();
    }
}
