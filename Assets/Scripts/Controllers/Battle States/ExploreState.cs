﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploreState : BattleState {

    public override void Enter()
    {
        base.Enter();
        RefreshPrimaryStatPanel(Pos);
    }

    public override void Exit()
    {
        base.Exit();
        StatPanelController.HidePrimary();
    }

    protected override void OnMove(object sender, object args)
    {
        SelectTile(Pos + (Point)args);
        RefreshPrimaryStatPanel(Pos);
    }

    protected override void OnFire(object sender, object args)
    {
        if ((int)args == 0)
        {
            owner.ChangeState<CommandSelectionState>();
        }
    }
}
