﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityTargetState : BattleState {

	private List<Tile> tiles;
    private AbilityRange ability;

    #region Overrides
    public override void Enter()
    {
        base.Enter();
        ability = Turn.Ability.GetComponent<AbilityRange>();
        SelectTiles();
        StatPanelController.ShowPrimary(Turn.Actor.gameObject);
        
        if (ability.DirectionalOriented)
            RefreshSecondaryStatPanel(Pos);
    }

    public override void Exit()
    {
        base.Exit();
        Board.DeselectTiles(tiles);
        StatPanelController.HidePrimary();
        StatPanelController.HideSecondary();
    }

    protected override void OnMove(object sender, object args)
    {
        Point pos = (Point)args;

        if (ability.DirectionalOriented)
        {
            ChangeDirection(pos);
        }
        else
        {
            SelectTile(Pos + pos);
            RefreshSecondaryStatPanel(Pos);
        }
    }

    protected override void OnFire(object sender, object args)
    {
        int button = (int)args;

        if (button == 0)
        {
            if (ability.DirectionalOriented || tiles.Contains(Board.GetTile(Pos)))
                owner.ChangeState<ConfirmAbilityTargetState>();
        }
        else
        {
            owner.ChangeState<CategorySelectionState>();
        }
    }
    #endregion

    #region Private
    private void ChangeDirection(Point p)
    {
        Directions dir = p.GetDirection();

        if (Turn.Actor.Dir != dir)
        {
            Board.DeselectTiles(tiles);
            Turn.Actor.Dir = dir;
            Turn.Actor.Match();
            SelectTiles();
        }
    }

    private void SelectTiles()
    {
        tiles = ability.GetTilesInRange(Board);
        Board.SelectTiles(tiles);
    }
    #endregion
}
