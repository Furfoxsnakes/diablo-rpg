﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitBattleState : BattleState {

    public override void Enter()
    {
        base.Enter();
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        Board.Load(LevelData);
        // start at the first tile in the list of tiles
        Point p = new Point((int)LevelData.Tiles[0].x, (int)LevelData.Tiles[0].z);
        SelectTile(p);
        SpawnTestUnits();
        yield return null;
        owner.ChangeState<SelectUnitState>();
    }

    private void SpawnTestUnits()
    {
        System.Type[] components = new System.Type[]
        {
            typeof(WalkMovement), typeof(FlyMovement), typeof(TeleportMovement)
        };

        string[] jobs = new string[] {"Rogue", "Warrior", "Wizard" };

        for (int i = 0; i < jobs.Length; i++)
        {
            GameObject instance = Instantiate(owner.HeroPrefab) as GameObject;
            instance.name = jobs[i];

            Stats s = instance.AddComponent<Stats>();
            s[StatTypes.LVL] = 1;

            GameObject jobPrefab = Resources.Load<GameObject>("Jobs/" + jobs[i]);
            GameObject jobInstance = Instantiate(jobPrefab) as GameObject;
            jobInstance.transform.SetParent(instance.transform);

            Job job = jobInstance.GetComponent<Job>();
            job.Employ();
            job.LoadDefaultStats();

            var tile = LevelData.Tiles[Random.Range(0, LevelData.Tiles.Count)];
            int randPosX = (int)tile.x;
            int randPosY = (int)tile.z;
            Point p = new Point(randPosX, randPosY);

            Unit unit = instance.GetComponent<Unit>();
            unit.Place(Board.GetTile(p));
            unit.Match();

            instance.AddComponent<WalkMovement>();

            Units.Add(unit);

            Rank rank = instance.AddComponent<Rank>();
            rank.Init(10);
        }
    }
}
