﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetState : BattleState {

    List<Tile> tiles;

    public override void Enter()
    {
        base.Enter();
        Movement mover = owner.Turn.Actor.GetComponent<Movement>();
        tiles = mover.GetTilesInRange(Board);
        Board.SelectTiles(tiles);
    }

    public override void Exit()
    {
        base.Exit();
        Board.DeselectTiles(tiles);
        tiles = null;
    }

    protected override void OnMove(object sender, object args)
    {
        SelectTile(Pos + (Point)args);
    }

    protected override void OnFire(object sender, object args)
    {
        if ((int)args == 0)
        {
            // only move if within valid tiles
            if (tiles.Contains(owner.CurrentTile))
                owner.ChangeState<MoveSequenceState>();
        }
        else
        {
            owner.ChangeState<CommandSelectionState>();
        }
    }
}
