﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndFacingState : BattleState {

    #region Fields
    Directions startDir;
    #endregion

    #region Overrides
    public override void Enter()
    {
        base.Enter();
        startDir = Turn.Actor.Dir;
        SelectTile(Turn.Actor.Tile.Pos);
    }

    protected override void OnMove(object sender, object args)
    {
        Point p = (Point)args;

        Turn.Actor.Dir = p.GetDirection();
        Turn.Actor.Match();
    }

    protected override void OnFire(object sender, object args)
    {
        int button = (int)args;

        switch (button)
        {
            case 0: // LMB
                owner.ChangeState<SelectUnitState>();
                break;
            case 1: // RMB
                Turn.Actor.Dir = startDir;
                Turn.Actor.Match();
                owner.ChangeState<CommandSelectionState>();
                break;
        }
    }
    #endregion
}
