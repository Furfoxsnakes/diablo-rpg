﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandSelectionState : BaseAbilityMenuState
{
    public override void Enter()
    {
        base.Enter();
        StatPanelController.ShowPrimary(Turn.Actor.gameObject);
    }

    public override void Exit()
    {
        base.Exit();
        StatPanelController.HidePrimary();
    }

    protected override void Cancel()
    {
        // Undo move
        if (Turn.HasUnitMoved && Turn.LockMove)
        {
            Turn.UndoMove();
            AbilityMenuPanelController.SetLocked(0, false);
            SelectTile(Turn.Actor.Tile.Pos);
        }
        else
        {
            owner.ChangeState<ExploreState>();
        }
    }

    protected override void Confirm()
    {
        switch (AbilityMenuPanelController.Selection)
        {
            case 0:     // Move
                owner.ChangeState<MoveTargetState>();
                break;
            case 1:     // Action
                owner.ChangeState<CategorySelectionState>();
                break;
            case 2:     // Wait
                owner.ChangeState<EndFacingState>();
                break;
        }
    }

    protected override void LoadMenu()
    {
        if (menuOptions == null)
        {
            menuTitle = "Commands";
            menuOptions = new List<string>(3)
            {
                "Move",
                "Action",
                "Wait"
            };
        }

        // display the ability menu and lock the approriate entired if applicable
        AbilityMenuPanelController.Show(menuTitle, menuOptions);
        AbilityMenuPanelController.SetLocked(0, Turn.HasUnitMoved);
        AbilityMenuPanelController.SetLocked(1, Turn.HasUnitActed);
    }
}
