﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSequenceState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        StartCoroutine(Sequence());
    }

    private IEnumerator Sequence()
    {
        Movement m = owner.Turn.Actor.GetComponent<Movement>();
        yield return StartCoroutine(m.Traverse(owner.CurrentTile));
        Turn.HasUnitMoved = true;
        owner.ChangeState<CommandSelectionState>();
    }
}
