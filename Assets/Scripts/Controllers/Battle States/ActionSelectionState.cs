﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSelectionState : BaseAbilityMenuState
{
    public static int Category;

    public override void Enter()
    {
        base.Enter();
        StatPanelController.ShowPrimary(Turn.Actor.gameObject);
    }

    public override void Exit()
    {
        base.Exit();
        StatPanelController.HidePrimary();
    }

    private readonly string[] whiteMagicOptions = new string[]
    {
        "Cure",
        "Raise",
        "Holy"
    };
    private readonly string[] blackMagicOptions = new string[]
    {
        "Fire",
        "Ice",
        "Lightning"
    };

    protected override void Cancel()
    {
        owner.ChangeState<CategorySelectionState>();
    }

    protected override void Confirm()
    {
        Turn.Ability = Turn.Actor.GetComponentInChildren<AbilityRange>().gameObject;
        owner.ChangeState<AbilityTargetState>();
    }

    protected override void LoadMenu()
    {
        if (menuOptions == null)
            menuOptions = new List<string>();

        if (Category == 0)
        {
            menuTitle = "White Magic";
            SetOptions(whiteMagicOptions);
        }
        else
        {
            menuTitle = "Black Magic";
            SetOptions(blackMagicOptions);
        }

        AbilityMenuPanelController.Show(menuTitle, menuOptions);
    }

    private void SetOptions(string[] options)
    {
        menuOptions.Clear();
        for (int i = 0; i < options.Length; ++i)
            menuOptions.Add(options[i]);
    }
}
