﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleState : State {

    protected BattleController owner;
    public CameraRig CameraRig { get { return owner.CameraRig; } }
    public Board Board { get { return owner.Board; } }
    public LevelData LevelData { get { return owner.LevelData; } }
    public Transform TileSelectionIndicator { get { return owner.TileSelectionIndicator; } }
    public Point Pos { get { return owner.Pos; } set { owner.Pos = value; } }
    public AbilityMenuPanelController AbilityMenuPanelController { get { return owner.AbilityMenuPanelController;}}
    public StatPanelController StatPanelController { get { return owner.StatPanelController;}}
    public Turn Turn { get { return owner.Turn;}}
    public List<Unit> Units { get { return owner.Units;}}

    protected virtual void Awake()
    {
        owner = GetComponent<BattleController>();
    }

    protected override void AddListeners()
    {
        this.AddObserver(OnMove, "MoveEvent");
        this.AddObserver(OnFire, "FireEvent");
    }

    protected override void RemoveListeners()
    {
        this.RemoveObserver(OnMove, "MoveEvent");
        this.RemoveObserver(OnFire, "FireEvent");
    }

    protected virtual void OnMove(object sender, object args)
    {

    }

    protected virtual void OnFire(object sender, object args)
    {

    }

    protected virtual void SelectTile (Point p)
    {
        // don't do anything if the tile is already selected or the tile doesn't exist in the list
        if (Pos == p || !Board.Tiles.ContainsKey(p))
            return;
        Pos = p;
        TileSelectionIndicator.localPosition = Board.Tiles[p].Center;
    }

    protected virtual Unit GetUnit(Point p)
    {
        Tile t = Board.GetTile(p);
        GameObject content = t != null ? t.Content : null;
        return content != null ? content.GetComponent<Unit>() : null;
    }

    protected virtual void RefreshPrimaryStatPanel (Point p)
    {
        Unit target = GetUnit(p);
        
        if (target != null)
            StatPanelController.ShowPrimary(target.gameObject);
        else
            StatPanelController.HidePrimary();
    }

    protected virtual void RefreshSecondaryStatPanel(Point p)
    {
        Unit target = GetUnit(p);

        if (target != null)
            StatPanelController.ShowSecondary(target.gameObject);
        else
            StatPanelController.HideSecondary();
    }
}
