﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

class Repeater
{
    const float threshold = 0.5f;
    const float rate = 0.25f;
    float _next;
    bool _hold;
    string _axis;
    public Repeater(string axisName)
    {
        _axis = axisName;
    }
    public int Update()
    {
        int retValue = 0;
        int value = Mathf.RoundToInt(Input.GetAxisRaw(_axis));
        if (value != 0)
        {
            if (Time.time > _next)
            {
                retValue = value;
                _next = Time.time + (_hold ? rate : threshold);
                _hold = true;
            }
        }
        else
        {
            _hold = false;
            _next = 0;
        }
        return retValue;
    }
}

public class InputController : MonoBehaviour {

    private readonly Repeater hor = new Repeater("Horizontal");
    private readonly Repeater ver = new Repeater("Vertical");
    private readonly string[] buttons = new string[] { "Fire1", "Fire2", "Fire3" };
	
	// Update is called once per frame
	void Update () {

        // movement
        int x = hor.Update();
        int y = ver.Update();

        if (x != 0 || y != 0)
            this.PostNotification("MoveEvent", new Point(x, y));

        // mouse fire
        for (int i = 0; i < 3; i++)
            if (Input.GetButtonUp(buttons[i]))
                this.PostNotification("FireEvent", i);
	}
}
