﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Party = System.Collections.Generic.List<UnityEngine.GameObject>;

public abstract class ExperienceManager {

	private const float MIN_LEVEL_BONUS = 1.5f;
    private const float MAX_LEVEL_BONUS = 0.5f;

    public static void AwardExperience (int amount, Party party)
    {
        List<Rank> ranks = new List<Rank>(party.Count);

        for (int i = 0; i < party.Count; ++i)
        {
            Rank r = party[i].GetComponent<Rank>();

            if (r != null)
                ranks.Add(r);
        }

        // determine the range of the actor level stats
        int min = int.MaxValue;
        int max = int.MinValue;

        for (int i = ranks.Count - 1; i >= 0; --i)
        {
            min = Mathf.Min(ranks[i].LVL, min);
            max = Mathf.Max(ranks[i].LVL, max);
        }

        // weigh the amount to award based on their level
        float[] weights = new float[party.Count];
        float summedWeights = 0;

        for (int i = ranks.Count - 1; i >= 0; --i)
        {
            float percent = (float)(ranks[i].LVL - min / (float)(max - min));
            weights[i] = Mathf.Lerp(MIN_LEVEL_BONUS, MAX_LEVEL_BONUS, percent);
            summedWeights += weights[i];
        }

        // hand out the weighted award
        for (int i = ranks.Count - 1; i >= 0; --i)
        {
            int subAmount = Mathf.FloorToInt((weights[i] / summedWeights) * amount);
            ranks[i].EXP += subAmount;
        }
    }
}
