﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationController : MonoBehaviour {

    [SerializeField] ConversationPanel leftPanel;
    [SerializeField] ConversationPanel rightPanel;
    Canvas canvas;
    IEnumerator conversation;
    Tweener transition;

    const string ShowTop = "Show Top";
    const string ShowBottom = "Show Bottom";
    const string HideTop = "Hide Top";
    const string HideBottom = "Hide Bottom";

    public static event EventHandler CompleteEvent;

    private void Start()
    {
        canvas = GetComponentInChildren<Canvas>();

        // panel position fallbacks if not inherintly set
        if (leftPanel.Panel.CurrentPos == null)
            leftPanel.Panel.SetPosition(HideBottom, false);
        if (rightPanel.Panel.CurrentPos == null)
            rightPanel.Panel.SetPosition(HideBottom, false);

        // disable the canvas until needed to save processing power
        canvas.gameObject.SetActive(false);
    }

    public void Show(ConversationData data)
    {
        canvas.gameObject.SetActive(true);
        conversation = Sequence(data);
        conversation.MoveNext();
    }

    public void Next()
    {
        // don't do anything if there is no conversation left or still in transition
        if (conversation == null || transition != null)
            return;

        conversation.MoveNext();
    }

    IEnumerator Sequence(ConversationData data)
    {
        for (int i = 0; i < data.list.Count; ++i)
        {
            SpeakerData sd = data.list[i];

            // get the proper panel based on the speaker data's anchor
            ConversationPanel currentPanel = (sd.anchor == TextAnchor.UpperLeft || sd.anchor == TextAnchor.MiddleLeft || sd.anchor == TextAnchor.LowerLeft) ? leftPanel : rightPanel;

            // get a reference to the display sequence on the panel and start the sequence
            IEnumerator presenter = currentPanel.Display(sd);
            presenter.MoveNext();

            string show, hide;

            if (sd.anchor == TextAnchor.UpperLeft || sd.anchor == TextAnchor.UpperCenter || sd.anchor == TextAnchor.UpperRight)
            {
                show = ShowTop;
                hide = HideTop;
            }
            else
            {
                show = ShowBottom;
                hide = HideBottom;
            }

            currentPanel.Panel.SetPosition(hide, false);
            MovePanel(currentPanel, show);

            // wait for a frame and then move to the next part of the sequence
            yield return null;
            while (presenter.MoveNext())
                yield return null;

            // hide the panel and then start the next part of the sequence when it's done animating
            MovePanel(currentPanel, hide);
            transition.easingControl.completedEvent += delegate(object sender, EventArgs e){
                conversation.MoveNext();
            };

            yield return null;
        }

        // disable the canvas and clear the delegates to free up resources
        canvas.gameObject.SetActive(false);
        if (CompleteEvent != null)
            CompleteEvent(this, EventArgs.Empty);
    }

    void MovePanel(ConversationPanel obj, string pos)
    {
        transition = obj.Panel.SetPosition(pos, true);
        transition.easingControl.duration = 0.5f;
        transition.easingControl.equation = EasingEquations.EaseOutQuad;
    }
}
