﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : StateMachine {

    public CameraRig CameraRig;
    public Board Board;
    public LevelData LevelData;
    public Transform TileSelectionIndicator;
    public Point Pos;
    public GameObject HeroPrefab;
    public Tile CurrentTile { get { return Board.GetTile(Pos); } }
    public AbilityMenuPanelController AbilityMenuPanelController;
    public StatPanelController StatPanelController;
    public Turn Turn = new Turn();
    public List<Unit> Units = new List<Unit>();

    private void Start()
    {
        ChangeState<InitBattleState>();
    }
}
