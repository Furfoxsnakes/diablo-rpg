﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour {

    public virtual State CurrentState { get { return currentState; } set { Transition(value); } }
    State currentState;
    bool inTransition;

    /// <summary>
    /// Returns a requested state or adds state if not present
    /// </summary>
    /// <typeparam name="T">State</typeparam>
    /// <returns></returns>
    public virtual T GetState<T>() where T : State
    {
        T target = GetComponent<T>();
        if (target == null)
            target = gameObject.AddComponent<T>();
        return target;
    }

    /// <summary>
    /// Changes the current state
    /// </summary>
    /// <typeparam name="T">State</typeparam>
    public virtual void ChangeState<T>() where T : State
    {
        CurrentState = GetState<T>();
    }

    protected virtual void Transition(State value)
    {
        // don't do anything if the requested state is already set as the current or if in transition
        if (currentState == value || inTransition)
            return;

        inTransition = true;

        // run the exit function on the current state
        if (currentState != null)
            currentState.Exit();

        currentState = value;

        // run the enter function on the new state
        if (currentState != null)
            currentState.Enter();

        inTransition = false;
    }
}
