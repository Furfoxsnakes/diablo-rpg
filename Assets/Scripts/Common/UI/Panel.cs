﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(LayoutAnchor))]
public class Panel : MonoBehaviour {

    [System.Serializable]
    public class Position
    {
        public string Name;
        public TextAnchor MyAnchor;
        public TextAnchor ParentAnchor;
        public Vector2 Offset;

        public Position(string name)
        {
            Name = name;
        }

        public Position(string name, TextAnchor myAnchor, TextAnchor parentAnchor) : this(name)
        {
            MyAnchor = myAnchor;
            ParentAnchor = parentAnchor;
        }

        public Position(string name, TextAnchor myAnchor, TextAnchor parentAnchor, Vector2 offset) : this(name, myAnchor, parentAnchor)
        {
            Offset = offset;
        }
    }

    [SerializeField] private List<Position> positionList;
    private Dictionary<string, Position> positionMap;
    private LayoutAnchor anchor;

    public Position CurrentPos { get; private set; }
    public Tweener Transition { get; private set; }
    public bool InTransition { get { return Transition != null; } }

    public Position this[string name]
    {
        get
        {
            if (positionMap.ContainsKey(name))
                return positionMap[name];
            return null;
        }
    }

    private void Awake()
    {
        anchor = GetComponent<LayoutAnchor>();
        positionMap = new Dictionary<string, Position>(positionList.Count);
        for (int i = 0; i < positionList.Count; ++i)
            AddPosition(positionList[i]);
    }

    private void Start()
    {
        if (CurrentPos == null && positionList.Count > 0)
            SetPosition(positionList[0], false);
    }

    public void AddPosition(Position p)
    {
        positionMap[p.Name] = p;
    }

    public void RemovePosition(Position p)
    {
        if (positionMap.ContainsKey(p.Name))
            positionMap.Remove(p.Name);
    }

    public Tweener SetPosition (string posName, bool animated)
    {
        return SetPosition(positionMap[posName], animated);
    }

    public Tweener SetPosition(Position p, bool animated)
    {
        CurrentPos = p;
        if (CurrentPos == null)
            return null;

        if (InTransition)
            Transition.easingControl.Stop();

        if (animated)
        {
            Transition = anchor.MoveToAnchorPosition(p.MyAnchor, p.ParentAnchor, p.Offset);
            return Transition;
        }
        else
        {
            anchor.SnapToAnchorPosition(p.MyAnchor, p.ParentAnchor, p.Offset);
            return null;
        }
    }
}
