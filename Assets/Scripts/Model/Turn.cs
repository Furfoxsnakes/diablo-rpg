﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn {

    public Unit Actor;
    public bool HasUnitMoved;
    public bool HasUnitActed;
    public bool LockMove;
    public GameObject Ability;
    public List<Tile> targets;
    Tile startTile;
    Directions startDir;

    public void Change(Unit current)
    {
        Actor = current;
        HasUnitMoved = false;
        HasUnitActed = false;
        LockMove = false;
        startTile = Actor.Tile;
        startDir = Actor.Dir;
    }

    public void UndoMove()
    {
        HasUnitMoved = false;
        Actor.Place(startTile);
        Actor.Dir = startDir;
        Actor.Match();
    }
}
