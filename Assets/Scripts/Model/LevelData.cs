﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Map/Level Data")]
public class LevelData : ScriptableObject {

    public List<Vector3> Tiles;
}
