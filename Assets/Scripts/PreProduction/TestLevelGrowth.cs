﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Party = System.Collections.Generic.List<UnityEngine.GameObject>;

public class TestLevelGrowth : MonoBehaviour {

    private void OnEnable()
    {
        this.AddObserver(OnLevelChange, Stats.DidChangeNotification(StatTypes.LVL));
        this.AddObserver(OnExperienceException, Stats.WillChangeNotification(StatTypes.EXP));
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnLevelChange, Stats.DidChangeNotification(StatTypes.LVL));
        this.RemoveObserver(OnExperienceException, Stats.WillChangeNotification(StatTypes.EXP));
    }

    // Use this for initialization
    void Start () {
		VerifyLevelToExperienceCalc();
        VerifySharedExperienceDist();
	}

    private void VerifyLevelToExperienceCalc()
    {
        for (int i = 1; i < 100; ++i)
        {
            int expLvl = Rank.ExperienceForLevel(i);
            int lvlExp = Rank.LevelForExperience(expLvl);

            if (lvlExp != i)
                Debug.Log(string.Format("Mismatch on level:{0} with exp:{1} returned:{2}", i, expLvl, lvlExp));
            else
                Debug.Log(string.Format("Level:{0} = Exp:{1}", lvlExp, expLvl));
        }
    }

    private void VerifySharedExperienceDist()
    {
        string[] names = new string[] {"Mr Macholo", "Foxysocks", "Furfoxsnakes", "Homeloaf"};

        Party party = new Party();

        for (int i = 0; i < 4; ++i)
        {
            GameObject actor = new GameObject(names[i]);
            actor.AddComponent<Stats>();
            Rank rank = actor.AddComponent<Rank>();
            rank.Init(Random.Range(1,5));
            party.Add(actor);
        }

        Debug.Log("===== Before Adding Experience ======");
        LogParty(party);

        Debug.Log("=====================================");
        ExperienceManager.AwardExperience(1000, party);

        Debug.Log("===== After Adding Experience ======");
        LogParty(party);
    }
	
	private void OnLevelChange(object sender, object args)
    {
        Stats stats = sender as Stats;
        Debug.Log(stats.name + " leveled up!");
    }

    private void LogParty(Party p)
    {
        for (int i = 0; i < p.Count; ++i)
        {
            GameObject actor = p[i];
            Rank rank = actor.GetComponent<Rank>();
            Debug.Log(string.Format("Name: {0} Level:{1} XP:{2}", actor.name, rank.LVL, rank.EXP));
        }
    }

    private void OnExperienceException(object sender, object args)
    {
        GameObject actor = (sender as Stats).gameObject;
        ValueChangeException vce = args as ValueChangeException;

        int roll = Random.Range(0, 5);

        switch (roll)
        {
            case 0: // stop the receiving of xp
                vce.FlipToggle();
                Debug.Log(string.Format("{0} would have received {1} experience, but it was stopped", actor.name, vce.delta));
                break;
            case 1: // add 1000 experience
                vce.AddModifier(new AddValueModifier(0, 1000));
                Debug.Log(string.Format("{0} would have received {1} experience, but 1000 more was added", actor.name, vce.delta));
                break;
            case 2: // double the experience
                vce.AddModifier(new MultiplyValueModifier(0, 2f));
                Debug.Log(string.Format("{0} would have received {1} experience, but it was doubled", actor.name, vce.delta));
                break;
            default:
                Debug.Log(string.Format("{0} will receive {1} experience", actor.name, vce.delta));
                break;
        }
    }
}
