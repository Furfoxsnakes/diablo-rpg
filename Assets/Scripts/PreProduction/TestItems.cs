﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestItems : MonoBehaviour {

	List<GameObject> inventory = new List<GameObject>();
    List<GameObject> combatants = new List<GameObject>();

    private void Start()
    {
        CreateItems();
        CreateCombatants();
        StartCoroutine(SimulateBattle());
    }

    private void OnEnable()
    {
        this.AddObserver(OnEquippedItem, Equipment.EQUIPPED_NOTIFICATION);
        this.AddObserver(OnUnEquippedItem, Equipment.UNEQUIPPED_NOTIFICATION);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnEquippedItem, Equipment.EQUIPPED_NOTIFICATION);
        this.RemoveObserver(OnUnEquippedItem, Equipment.UNEQUIPPED_NOTIFICATION);
    }

    private void OnEquippedItem (object sender, object args)
    {
        Equipment equip = sender as Equipment;
        Equippable item = args as Equippable;
        inventory.Remove(item.gameObject);
        Debug.Log(string.Format("{0} equipped {1}", equip.name, item.name));
    }

    private void OnUnEquippedItem(object sender, object args)
    {
        Equipment equip = sender as Equipment;
        Equippable item = args as Equippable;
        inventory.Add(item.gameObject);
        Debug.Log(string.Format("{0} unequipped {1}", equip.name, item.name));
    }

    private GameObject CreateItem(string title, StatTypes type, int amount)
    {
        GameObject item = new GameObject(title);
        StatModifierFeature smf = item.AddComponent<StatModifierFeature>();
        smf.type = type;
        smf.amount = amount;
        return item;
    }

    private GameObject CreateConsumableItem (string title, StatTypes type, int amount)
    {
        GameObject item = CreateItem(title, type, amount);
        item.AddComponent<Consumable>();
        return item;
    }

    private GameObject CreateEquippableItem (string title, StatTypes type, int amount, EquipSlots slot)
    {
        GameObject item = CreateItem(title, type, amount);
        Equippable equip = item.AddComponent<Equippable>();
        equip.DefaultSlots = slot;
        return item;
    }

    private GameObject CreateActor (string name)
    {
        GameObject actor = new GameObject(name);
        Stats s = actor.AddComponent<Stats>();
        s[StatTypes.HP] = s[StatTypes.MHP] = Random.Range(500, 1000);
        s[StatTypes.ATK] = Random.Range(30, 50);
        s[StatTypes.DEF] = Random.Range(30, 50);
        return actor;
    }

    private GameObject CreateHero()
    {
        GameObject actor = CreateActor("Hero");
        actor.AddComponent<Equipment>();
        return actor;
    }

    private void CreateItems()
    {
        inventory.Add(CreateConsumableItem("Health Potion", StatTypes.HP, 300));
        inventory.Add(CreateConsumableItem("Bomb", StatTypes.HP, -150));
        inventory.Add(CreateEquippableItem("Sword", StatTypes.ATK, 10, EquipSlots.Primary));
        inventory.Add(CreateEquippableItem("Broad Sword", StatTypes.ATK, 15, (EquipSlots.Primary | EquipSlots.Secondary)) );
        inventory.Add(CreateEquippableItem("Shield", StatTypes.DEF, 10, EquipSlots.Secondary));
    }

    private void CreateCombatants()
    {
        combatants.Add(CreateHero());
        combatants.Add(CreateActor("Monster"));
    }

    IEnumerator SimulateBattle()
    {
        while (!VictoryCheck())
        {
            LogCombatants();
            HeroTurn();
            EnemyTurn();
            yield return new WaitForSeconds(1);
        }
        LogCombatants();
        Debug.Log("Battle Completed!");
    }

    private bool VictoryCheck()
    {
        for (int i = 0; i < 2; ++i)
        {
            Stats s = combatants[i].GetComponent<Stats>();
            if (s[StatTypes.HP] <= 0)
                return true;
        }
        return false;
    }

    private void LogCombatants()
    {
        Debug.Log("============");
        for (int i = 0; i < 2; ++i)
            LogToConsole(combatants[i]);
        Debug.Log("============");
    }

    private void LogToConsole(GameObject actor)
    {
        Stats s = actor.GetComponent<Stats>();
        Debug.Log(string.Format("Name:{0} HP:{1}/{2} ATK:{3} DEF:{4}", actor.name, s[StatTypes.HP], s[StatTypes.MHP], s[StatTypes.ATK], s[StatTypes.DEF]));
    }

    private void EnemyTurn()
    {
        Attack(combatants[1], combatants[0]);
    }

    private void HeroTurn()
    {
        int rnd = Random.Range(0, 2);

        switch (rnd)
        {
            case 0:
                Attack(combatants[0], combatants[1]);
                break;
            default:
                UseInventory();
                break;
        }
    }

    private void UseInventory()
    {
        int rnd = Random.Range(0, inventory.Count);

        GameObject item = inventory[rnd];
        if (item.GetComponent<Consumable>() != null)
            ConsumeItem(item);
        else
            EquipItem(item);
    }

    private void EquipItem(GameObject item)
    {
        Debug.Log("Perhaps this will help...");
        Equippable toEquip = item.GetComponent<Equippable>();
        Equipment equip = combatants[0].GetComponent<Equipment>();
        equip.Equip(toEquip, toEquip.DefaultSlots);
    }

    private void ConsumeItem(GameObject item)
    {
        inventory.Remove(item);
        StatModifierFeature smf = item.GetComponent<StatModifierFeature>();

        if (smf.amount > 0)
        {
            // healing potion
            item.GetComponent<Consumable>().Consume(combatants[0]);
            Debug.Log("Ah... a potion!");
        }
        else
        {
            // damaging consumable such as a bomb
            item.GetComponent<Consumable>().Consume(combatants[1]);
            Debug.Log("Take that you stupid monster!");
        }

    }

    private void Attack(GameObject attacker, GameObject defender)
    {
        Stats s1 = attacker.GetComponent<Stats>();
        Stats s2 = defender.GetComponent<Stats>();
        int damage = Mathf.FloorToInt((s1[StatTypes.ATK] * 4 - s2[StatTypes.DEF] * 2) * Random.Range(0.9f, 1.1f));
        s2[StatTypes.HP] -= damage;
        Debug.Log(string.Format("{0} hits {1} for {2} damage!", attacker.name, defender.name, damage));
    }
}
