﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnchorPosition {

	TopLeft,
    TopCenter,
    TopRight,
    MiddleLeft,
    MiddleCenter,
    MiddleRight,
    BottomLeft,
    BottomCenter,
    BottomRight
}
