﻿[System.Flags]
public enum EquipSlots {

	None = 0,
    Primary = 1 << 0,       // main hand
    Secondary = 1 << 1,     // off hand
    Head = 1 << 2,          // helm
    Body = 1 << 3,          // body armour
    Accessory = 1 << 4      // ring, belt amulet
}
