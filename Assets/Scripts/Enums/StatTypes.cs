﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatTypes {

	LVL,    // Level
    EXP,    // Experience
    HP,     // Hit Points
    MHP,    // Max Hit Points
    MP,     // Mana Points
    MMP,    // Max Mana Points
    ATK,    // Physical Attack
    DEF,    // Defense
    MAT,    // Magic Attack
    MDF,    // Magic Defense
    EVD,    // Evade
    RES,    // Resistance
    SPD,    // Speed
    MOV,    // Move Range
    Count
}
