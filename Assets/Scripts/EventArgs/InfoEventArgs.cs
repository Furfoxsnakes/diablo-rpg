﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InfoEventArgs<T> : EventArgs {

    public T Info;

    public InfoEventArgs()
    {
        Info = default(T);
    }

    public InfoEventArgs(T info)
    {
        Info = info;
    }
}
