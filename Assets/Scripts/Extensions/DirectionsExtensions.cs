﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DirectionsExtensions {

    /// <summary>
    /// Get cardinal direction from tile to requested tile
    /// </summary>
    /// <param name="t1">Extended tile</param>
    /// <param name="t2">Tile to travel to</param>
    /// <returns>Direction</returns>
	public static Directions GetDirection(this Tile t1, Tile t2)
    {
        if (t1.Pos.y < t2.Pos.y) return Directions.North;   // t1 is below t2
        if (t1.Pos.x < t2.Pos.x) return Directions.East;    // t1 is left of t2
        if (t1.Pos.y > t2.Pos.y) return Directions.South;   // t1 is above t2
        return Directions.West;                             // fall through from above
    }
    public static Directions GetDirection(this Point p)
    {
        if (p.y > 0) return Directions.North;
        if (p.x > 0) return Directions.East;
        if (p.y < 0) return Directions.South;
        return Directions.West;
    }

    // North = 0 = 0
    // East = 1 = 90
    // South = 2 = 180
    // West = 3 = 270
    /// <summary>
    /// return the euler angle of a direction enum
    /// </summary>
    /// <param name="d">Direction</param>
    /// <returns>Euler angle</returns>
    public static Vector3 ToEuler (this Directions d)
    {
        return new Vector3(0, (int)d * 90, 0);
    }
}
