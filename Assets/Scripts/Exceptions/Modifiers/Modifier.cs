﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Modifier {

    public readonly int SortOrder;

    public Modifier(int sortOrder)
    {
        SortOrder = sortOrder;
    }
}
