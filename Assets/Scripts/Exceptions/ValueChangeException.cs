﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueChangeException : BaseException
{
    public readonly float FromValue;
    public readonly float ToValue;
    public float delta {get { return ToValue - FromValue;}}
    private List<ValueModifier> modifiers;

    public ValueChangeException(float fromValue, float toValue) : base(true)
    {
        FromValue = fromValue;
        ToValue = toValue;
    }

    public void AddModifier(ValueModifier vm)
    {
        if (modifiers == null)
            modifiers = new List<ValueModifier>();

        modifiers.Add(vm);
    }

    public float GetModifiedValue()
    {
        float value = ToValue;

        if (modifiers == null)  // no modifiers to apply
            return value;

        // sort by modifiers sort order
        modifiers.Sort(Compare);

        for (int i = 0; i < modifiers.Count; ++i)
            value = modifiers[i].Modify(value);

        return value;
    }

    private int Compare (ValueModifier x, ValueModifier y)
    {
        return x.SortOrder.CompareTo(y.SortOrder);
    }
}
